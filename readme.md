## Capas de la app ##
### Vista ###

La vista esta compuesta por 2 elementos el layout.blade.html que contiene el header y footer y commandline.blade.php que es una tarjeta con un input y la cual presenta el resultado y el ùltimo comando ejecutado.

### Persistencia ###

La persistencia de la información referente al cubo se esta realizado a tráves de sesiones.

### Aplicación ###

Compuesta por el controlador CubeController desde el cual al realizar el llamado al método process realiza la descomposición del comando dado y comunica la respuesta a la vista.